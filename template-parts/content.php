<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package USA_AFRICA
 */

?>
	<header class="entry-header">
	<div class="container">
			<div class="row">
				<div class="col-md-12">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				// usaafrica_posted_on();
				// usaafrica_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
		</div>
			</div>
		</div>
	</header><!-- .entry-header -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<?php usaafrica_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'usaafrica' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'usaafrica' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php usaafrica_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
</div>
	</div>
</div>
