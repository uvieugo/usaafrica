<?php
/**
 * Template part for displaying page content front-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package USA_AFRICA
 */

?>


		<?php
		the_content();
		?>

