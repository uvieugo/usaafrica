<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package USA_AFRICA
 */

?>

	</div><!-- #content -->


	<footer id="colophon" class="site-footer">
	<section class="footer bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <p>Africa Chamber of Commerce <br>
                        233 Erie Chicago IL <br>
                        60601
                    </p>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-5">
				<? 
								$custom_logo_id = get_theme_mod( 'custom_logo' );
								$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
								echo '<img src="'. $logo[0] . '" alt="" class="mx-auto d-block">';
							?>	
                        <!-- <img src="img/usaafric-full-logo.png" alt="" class="mx-auto d-block"> -->
                </div>
            </div>
        </div>
		<div class="text-center site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'usaafrica' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'usaafrica' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'usaafrica' ), 'usaafrica', '<a href="#">Daniel Uvietesivwi</a>' );
				?>
		</div><!-- .site-info -->
    </section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
