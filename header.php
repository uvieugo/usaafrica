<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package USA_AFRICA
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'usaafrica' ); ?></a>
	<section id="header" class="header">
        <nav class="navbar navbar-expand-lg ">
            <div class="container">
                <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
				<?php
					wp_nav_menu([
						'menu'            => 'top',
						'theme_location'  => 'menu-1',
						'container'       => 'div',
						'container_id'    => 'navbarSupportedContent',
						'container_class' => 'collapse navbar-collapse ',
						'menu_id'         => false,
						'menu_class'      => 'navbar-nav ml-auto',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					]);
					?>
            </div>

        </nav>
        <!-- <div class="header-cont">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header-img">
							<? 
								// $custom_logo_id = get_theme_mod( 'custom_logo' );
								// $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
								// echo '<img src="'. $logo[0] . '" alt="" class="mx-auto d-block">';
							?>	
                        </div>
                        <h1 class="header-text text-center">
						<?php 
							// $usaafrica_description = get_bloginfo( 'description', 'display' );
							// if ( $usaafrica_description || is_customize_preview() ) :
						?>
				
							<p class="site-description"> <?php //echo str_replace(' | ', '<br />', $usaafrica_description); /* WPCS: xss ok. */ ?></p>
						<?php //endif; ?>
                        </h1>
                    </div>
                </div>
            </div>

        </div> -->
</section>


	<!-- <header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$usaafrica_description = get_bloginfo( 'description', 'display' );
			if ( $usaafrica_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $usaafrica_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div>
</header> -->

	<div id="content" class="site-content">
